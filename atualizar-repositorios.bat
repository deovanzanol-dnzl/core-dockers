@echo on    
@echo '###########################################################''
@echo '#Script para atualizar os repositorios do projeto da rumo.#'
@echo '###########################################################'
@echo #Autor: Deovan Zanol                                      #
@echo '###########################################################'
@echo '###########################################################'
@echo '###########################################################'
@echo Atualizando sinistros.
cd ..\sinistros
git pull
@echo '###########################################################'
@echo Atualizando sro-frontend.
cd ..\sro-frontend 
git pull
@echo '###########################################################'
@echo Atualizando sindicancias.
cd ..\sindicancias 
git pull
@echo '###########################################################'
@echo Atualizando sindicanciamaterialrodante.
cd ..\sindicanciamaterialrodante 
git pull
@echo '###########################################################'
@echo Atualizando sindicanciaanalisestecnicas.
cd ..\sindicanciaanalisestecnicas 
git pull
@echo '###########################################################'
@echo Atualizando segurancapatrimonial.
cd ..\segurancapatrimonial 
git pull
@echo '###########################################################'
@echo Atualizando planoacao.
cd ..\planoacao 
git pull
@echo '###########################################################'
@echo Atualizando incidente.
cd ..\incidente 
git pull
@echo '###########################################################'
@echo Atualizando financeirosindicancia.
cd ..\financeirosindicancia 
git pull
@echo '###########################################################'
@echo '#Atualizado!                                              #'
@echo '###########################################################'
timeout 10