@echo on    
@echo ###########################################################
@echo #Script para atualizar os repositorios do projeto da rumo.#
@echo ###########################################################
@echo #Autor: Deovan Zanol                                      #
@echo ###########################################################
@echo ###########################################################
@echo Buildando funcionarios.
cd ..\funcionarios
docker build -f Dockerfile_build -t sro_backend_funcionarios .
@echo ###########################################################
@echo Buildando macros.
cd ..\macros
docker build -f Dockerfile_build -t sro_backend_macros .
@echo ###########################################################
@echo Buildando mercadorias.
cd ..\mercadorias
docker build -f Dockerfile_build -t sro_backend_mercadorias .
@echo ###########################################################
@echo Buildando notificações.
cd ..\notificacoes
docker build -f Dockerfile_build -t sro_backend_notificacoes .
@echo ###########################################################
@echo Buildando sinistros.
cd ..\sinistros
docker build -f Dockerfile_build -t sro_backend_sinistros .
@echo ###########################################################
@echo Buildando sro-frontend.
cd ..\sro-frontend 
docker build -t sro_frontend:qa-gft --build-arg configuration="qa-gft" .
@echo ###########################################################
@echo Buildando subdivisoes.
cd ..\subdivisoes
docker build -f Dockerfile_build -t sro_backend_subdivisoes .
@echo ###########################################################
@echo Buildando veiculosFerroviarios.
cd ..\veiculosFerroviarios 
docker build -f Dockerfile_build -t sro_backend_veiculosferroviarios .
@echo ###########################################################
@echo Buildando sindicancias.
cd ..\sindicancias 
docker build -f Dockerfile_build -t sro_backend_sindicancias .
@echo ###########################################################
@echo #Buildado com sucesso!                                  #
@echo ###########################################################
timeout 5