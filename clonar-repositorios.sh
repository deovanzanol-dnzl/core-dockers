#!/bin/bash
echo on    

echo ###########################################################
echo #Script para clonar os repositorios do projeto da rumo.   #
echo #Falta terminar                                           #
echo ###########################################################
echo #Autor: Deovan Zanol                                      #
echo ###########################################################
cd ..
echo ###########################################################
echo Clonando sinistros.
git clone git@bitbucket.org:rumo_logistica/sinistros.git
cd ./sinistros
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando sro-frontend.
git clone git@bitbucket.org:rumo_logistica/sro-frontend.git
cd ./sro-frontend
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando sindicancias.
git clone git@bitbucket.org:rumo_logistica/sindicancias.git
cd ./sindicancias
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando sindicanciamaterialrodante.
git clone git@bitbucket.org:rumo_logistica/sindicanciamaterialrodante.git
cd ./sindicanciamaterialrodante
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando sindicanciaanalisestecnicas.
git clone git@bitbucket.org:rumo_logistica/sindicanciaanalisestecnicas.git
cd ./sindicanciaanalisestecnicas
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando segurancapatrimonial.
git clone git@bitbucket.org:rumo_logistica/segurancapatrimonial.git
cd ./segurancapatrimonial
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando planoacao.
git clone git@bitbucket.org:rumo_logistica/planoacao.git
cd ./planoacao
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando Incidente.
git clone git@bitbucket.org:rumo_logistica/incidente.git
cd ./incidente
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando financeirosindicancia.
git clone git@bitbucket.org:rumo_logistica/financeirosindicancia.git
cd ./financeirosindicancia
git checkout QA-GFT
cd ..
echo ###########################################################
echo #Repositorios clonado com sucesso!                        #
echo ###########################################################
sleep 10s
