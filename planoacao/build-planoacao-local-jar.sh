#!/bin/bash
echo on    

echo '###########################################################'
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo '###########################################################'
echo #Autor: Deovan Zanol                                      #
echo '###########################################################'
echo '###########################################################'
echo Buildando planoacao.
cd ../../planoacao
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/planoacao/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker planoacao                             #'
echo '###########################################################'
cd './planoacao'
docker build -t planoacao .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s