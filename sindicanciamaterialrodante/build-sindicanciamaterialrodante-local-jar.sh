#!/bin/bash
echo on    

echo '###########################################################'
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo '###########################################################'
echo #Autor: Deovan Zanol                                      #
echo '###########################################################'
echo '###########################################################'
echo Buildando SindicanciaMaterialRodante.
cd ../../sindicanciamaterialrodante
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/sindicanciamaterialrodante/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker sindicanciamaterialrodante               #'
echo '###########################################################'
cd './sindicanciamaterialrodante'
docker build -t sindicanciamaterialrodante .
echo '###########################################################'
echo '#Repositorio buildado com sucesso!                        #'
echo '###########################################################'

sleep 10s