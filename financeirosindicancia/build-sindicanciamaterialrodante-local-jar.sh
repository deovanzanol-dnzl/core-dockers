#!/bin/bash
echo on    

echo '###########################################################'
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo '###########################################################'
echo #Autor: Deovan Zanol                                      #
echo '###########################################################'
echo '###########################################################'
echo Buildando financeirosindicancia.
cd ../../financeirosindicancia
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/financeirosindicancia/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker financeirosindicancia               #'
echo '###########################################################'
cd './financeirosindicancia'
docker build -t financeirosindicancia .
echo '###########################################################'
echo '#Repositorio buildado com sucesso!                        #'
echo '###########################################################'

sleep 10s