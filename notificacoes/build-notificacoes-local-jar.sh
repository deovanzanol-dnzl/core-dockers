#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Buildando notificacoes.
cd ../../notificacoes
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/notificacoes/app.jar
cd ../core-dockers-cdp
echo '###########################################################'
echo '#Building docker notificacoes                             #'
echo '###########################################################'
cd './notificacoes'
docker build -t notificacoes .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s