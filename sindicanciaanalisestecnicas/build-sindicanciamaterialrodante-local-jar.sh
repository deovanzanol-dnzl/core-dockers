#!/bin/bash
echo on    

echo '###########################################################'
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo '###########################################################'
echo #Autor: Deovan Zanol                                      #
echo '###########################################################'
echo '###########################################################'
echo '###########################################################'
echo Buildando SindicanciaAnalisesTecnicas.
cd ../../sindicanciaanalisestecnicas
mvn clean package -DskipTests  -Plocal
cp ./target/SindicanciaAnalisesTecnicas*.jar ../core-dockers/sindicanciaanalisestecnicas/SindicanciaAnalisesTecnicas.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker sindicanciaanalisestecnicas               #'
echo '###########################################################'
cd './sindicanciaanalisestecnicas'
docker build -t sindicanciaanalisestecnicas .
echo '###########################################################'
echo '#Repositorio buildado com sucesso!                        #'
echo '###########################################################'

sleep 10s