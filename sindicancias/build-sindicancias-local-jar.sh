#!/bin/bash
echo on    

echo '###########################################################'
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo '###########################################################'
echo #Autor: Deovan Zanol                                      #
echo '###########################################################'
echo '###########################################################'
echo Buildando sindicancias.
cd ../../sindicancias
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/sindicancias/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker sindicancias                             #'
echo '###########################################################'
cd './sindicancias'
docker build -t sindicancias .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s