#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Buildando sinistros.
cd ../../sinistros
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/sinistros/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker sinistros                             #'
echo '###########################################################'
cd './sinistros'
docker build -t sinistros .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s