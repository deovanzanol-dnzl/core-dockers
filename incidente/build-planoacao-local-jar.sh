#!/bin/bash
echo on    

echo '###########################################################'
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo '###########################################################'
echo #Autor: Deovan Zanol                                      #
echo '###########################################################'
echo '###########################################################'
echo Buildando incidente.
cd ../../incidente
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/incidente/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker incidente                             #'
echo '###########################################################'
cd './incidente'
docker build -t incidente .
echo '###########################################################'
echo '#Repositorio buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s