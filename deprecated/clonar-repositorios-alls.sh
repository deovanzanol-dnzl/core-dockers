#!/bin/bash
echo on    

echo ###########################################################
echo #Script para clonar os repositorios do projeto da rumo.   #
echo #Falta terminar                                           #
echo ###########################################################
echo #Autor: Deovan Zanol                                      #
echo ###########################################################

cd ..

echo ###########################################################
echo Clonando funcionarios.
git clone git@bitbucket.org:rumo_logistica/funcionarios.git

cd ./funcionarios
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando macros.
git clone git@bitbucket.org:rumo_logistica/macros.git
cd ./macros
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando mercadorias.
git clone git@bitbucket.org:rumo_logistica/mercadorias.git
cd ./mercadorias
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando notificacoes.
git clone git@bitbucket.org:rumo_logistica/notificacoes.git
cd ./notificacoes
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando sinistros.
git clone git@bitbucket.org:rumo_logistica/sinistros.git
cd ./sinistros
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando sro-frontend.
git clone git@bitbucket.org:rumo_logistica/sro-frontend.git
cd ./sro-frontend
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando subdivisoes.
git clone git@bitbucket.org:rumo_logistica/subdivisoes.git
cd ./subdivisoes
git checkout QA-GFT
cd ..
echo ###########################################################
echo Clonando veiculosferroviarios.
git clone git@bitbucket.org:rumo_logistica/veiculosferroviarios.git
cd ./veiculosferroviarios
git checkout QA-GFT
cd ..
@echo ###########################################################
@echo Clonando sindicancias.
git clone git@bitbucket.org:rumo_logistica/sindicancias.git
cd ./sindicancias
git checkout QA-GFT
cd ..
echo ###########################################################
echo #Repositorios clonado com sucesso!                        #
echo ###########################################################
sleep 10s
