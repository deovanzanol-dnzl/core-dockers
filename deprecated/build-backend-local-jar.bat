
@echo on    
@echo ###########################################################
@echo #Script para buildar os repositorios do projeto da rumo.  #
@echo #                                                         #
@echo #Build local                                             #
@echo ###########################################################
@echo #Autor: Deovan Zanol                                      #
@echo ###########################################################
@echo ###########################################################
@echo Buildando funcionarios.
cd ..\funcionarios
mvn clean package -DskipTests -Plocal && Xcopy /Y  ".\target\Funcionarios.jar" "..\core-dockers\funcionarios\Funcionarios.jar"

@echo ###########################################################
@echo Buildando macros.
cd ..\macros
mvn clean package -DskipTests -Plocal && Xcopy /Y   "target\Macros*.jar" "..\core-dockers\macros\Macros.jar"
@echo ###########################################################
@echo Buildando mercadorias.
cd ..\mercadorias
mvn clean package -DskipTests -Plocal && Xcopy /Y  "target\Mercadorias*.jar" "..\core-dockers\mercadorias\Mercadorias.jar"
@echo ###########################################################
@echo Buildando notificacoes.
cd ..\notificacoes
mvn clean package -DskipTests -Plocal && Xcopy /Y  "target\Notificacoes*.jar" "..\core-dockers\notificacoes\Notificacoes.jar"
@echo ###########################################################
@echo Buildando sinistros.
cd ..\sinistros
mvn clean package -DskipTests -Plocal && Xcopy /Y  "target\Sinistro*.jar" "..\core-dockers\sinistros\Sinistro.jar"
@echo ###########################################################
@echo Buildando subdivisoes.
cd ..\subdivisoes
mvn clean package -DskipTests -Plocal && Xcopy /Y  "target\Subdivisoes*.jar" "..\core-dockers\subdivisoes\Subdivisoes.jar"
@echo ###########################################################
@echo Buildando veiculosferroviarios.
cd ..\veiculosferroviarios
mvn clean package -DskipTests -Plocal && Xcopy /Y  "target\VeiculosFerroviarios*.jar" "..\core-dockers\veiculosferroviarios\VeiculosFerroviarios.jar"
@echo ###########################################################
@echo Buildando sindicancias.
cd ..\sindicancias
mvn clean package -DskipTests -Plocal && Xcopy /Y  "target\Sindicancias*.jar" "..\core-dockers\sindicancias\Sindicancias.jar"
@echo ###########################################################
@echo #Build concuido!                                          #
@echo ###########################################################
timeout 20