@echo on    
@echo ###########################################################
@echo #Script para atualizar os repositorios do projeto da rumo.#
@echo ###########################################################
@echo #Autor: Deovan Zanol                                      #
@echo ###########################################################
@echo ###########################################################
@echo Atualizando funcionarios.
cd ..\funcionarios
git pull
@echo ###########################################################
@echo Atualizando macros.
cd ..\macros
git pull
@echo ###########################################################
@echo Atualizando mercadorias.
cd ..\mercadorias
git pull
@echo ###########################################################
@echo Atualizando notificações.
cd ..\notificacoes
git pull
@echo ###########################################################
@echo Atualizando sinistros.
cd ..\sinistros
git pull
@echo ###########################################################
@echo Atualizando sro-frontend.
cd ..\sro-frontend 
git pull
@echo ###########################################################
@echo Atualizando subdivisoes.
cd ..\subdivisoes
git pull
@echo ###########################################################
@echo Atualizando veiculosFerroviarios.
cd ..\veiculosFerroviarios 
git pull
@echo ###########################################################
@echo Atualizando sindicancias.
cd ..\sindicancias 
git pull
@echo ###########################################################
@echo #Atualizado!                                  #
@echo ###########################################################
timeout 5