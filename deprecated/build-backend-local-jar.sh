#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Building local                                          #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Building funcionarios.
cd ../funcionarios
mvn clean package -DskipTests -Plocal
cp ./target/Funcionarios.jar ../core-dockers/funcionarios/Funcionarios.jar
echo '###########################################################'
echo Building macros.
cd ../macros
mvn clean package -DskipTests -Plocal
cp ./target/Macros*.jar ../core-dockers/macros/Macros.jar
echo '###########################################################'
echo Building mercadorias.
cd ../mercadorias
mvn clean package -DskipTests -Plocal  
cp ./target/Mercadorias*.jar ../core-dockers/mercadorias/Mercadorias.jar
echo '###########################################################'
echo Building notificacoes.
cd ../notificacoes
mvn clean package  -DskipTests -Plocal
cp target/Notificacoes*.jar ../core-dockers/notificacoes/Notificacoes.jar
echo '###########################################################'
echo Building sinistros.
cd ../sinistros
mvn clean package -DskipTests  -Plocal
cp ./target/Sinistro*.jar ../core-dockers/sinistros/Sinistro.jar
echo '###########################################################'
echo Building subdivisoes.
cd ../subdivisoes
mvn clean package -DskipTests -Plocal
cp target/Subdivisoes*.jar ../core-dockers/subdivisoes/Subdivisoes.jar
echo '###########################################################'
echo Building veiculosferroviarios.
cd ../veiculosferroviarios
mvn clean package -DskipTests -Plocal
cp ./target/VeiculosFerroviarios*.jar ../core-dockers/veiculosferroviarios/VeiculosFerroviarios.jar
echo '###########################################################'
echo Building sindicancias.
cd ../sindicancias
mvn clean package -DskipTests -Plocal
cp ./target/Sindicancias*.jar ../core-dockers/sindicancias/Sindicancias.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker funcionarios                             #'
echo '###########################################################'
cd './funcionarios'
docker build -t funcionarios .
echo '###########################################################'
echo '#Building docker macros                                   #'
echo '###########################################################'
cd '../macros'
docker build -t macros .
echo '###########################################################'
echo '#Building docker mercadorias                              #'
echo '###########################################################'
cd '../mercadorias'
docker build -t mercadorias .
echo '###########################################################'
echo '#Building docker sinistros                                #'
echo '###########################################################'
cd '../sinistros'
docker build -t sinistros .
echo '###########################################################'
echo '#Building docker subdivisoes                             #'
echo '###########################################################'
cd '../subdivisoes'
docker build -t subdivisoes .
echo '###########################################################'
echo '#Building docker veiculosferroviarios                     #'
echo '###########################################################'
cd '../veiculosferroviarios'
docker build -t veiculosferroviarios .
echo '###########################################################'
echo '#Building docker notificacoes                             #'
echo '###########################################################'
cd '../notificacoes'
docker build -t notificacoes .
echo '###########################################################'
echo  '#Building docker sindicancias                            #'
echo '###########################################################'
cd '../sindicancias'
docker build -t sindicancias .
echo '###########################################################'
echo '#Repositorios Building com sucesso!                       #'
echo '###########################################################'
sleep 10s