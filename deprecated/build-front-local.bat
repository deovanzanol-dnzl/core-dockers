@echo on    
@echo ###########################################################
@echo #Script para buildar os repositorios do projeto da rumo.  #
@echo #                                                         #
@echo #Build local                                             #
@echo ###########################################################
@echo #Autor: Deovan Zanol                                      #
@echo ###########################################################
@echo ###########################################################
@echo Buildando Frontend.
cd "..\sro-frontend"
npm install && npm run build -- --configuration=qa-gft --base-href=/web --deploy-url=/  && Xcopy /K /D /H /Y  "dist"  "..\core-dockers\sro frontend\dist"
copy "nginx-custom.conf"  "..\core-dockers\sro frontend\"
cd ..\core-dockers
@echo ###########################################################
@echo #Build concluido!                                         #
@echo ###########################################################
timeout 20