#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Building local                                           #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Building sinistros.
cd ../sinistros
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/sinistros/app.jar
echo '###########################################################'
echo Building sindicancias.
cd ../sindicancias
mvn clean package -DskipTests -Plocal
cp ./target/app*.jar ../core-dockers/sindicancias/app.jar
cd ../core-dockers
echo '###########################################################'
echo Building sindicanciamaterialrodante.
cd ../sindicanciamaterialrodante
mvn clean package -DskipTests -Plocal
cp ./target/app*.jar ../core-dockers/sindicanciamaterialrodante/app.jar
cd ../core-dockers
# echo '###########################################################'
# echo Building sindicanciaanalisestecnicas.
# cd ../sindicanciaanalisestecnicas
# mvn clean package -DskipTests -Plocal
# cp ./target/SindicanciaAnalisesTecnicas*.jar ../core-dockers/sindicanciaanalisestecnicas/SindicanciaAnalisesTecnicas.jar
# cd ../core-dockers
echo '###########################################################'
echo Building segurancapatrimonial.
cd ../segurancapatrimonial
mvn clean package -DskipTests -Plocal
cp ./target/app*.jar ../core-dockers/segurancapatrimonial/app.jar
cd ../core-dockers
echo '###########################################################'
echo Building planoacao.
cd ../planoacao
mvn clean package -DskipTests -Plocal
cp ./target/app*.jar ../core-dockers/planoacao/app.jar
cd ../core-dockers
echo '###########################################################'
echo Building incidente.
cd ../incidente
mvn clean package -DskipTests -Plocal
cp ./target/app*.jar ../core-dockers/incidente/app.jar
cd ../core-dockers
echo '###########################################################'
echo Building financeirosindicancia.
cd ../financeirosindicancia
mvn clean package -DskipTests -Plocal
cp ./target/app*.jar ../core-dockers/financeirosindicancia/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker sinistros                                #'
echo '###########################################################'
cd './sinistros'
docker build -t sinistros .
echo '###########################################################'
echo '#Building docker sindicancias                             #'
echo '###########################################################'
cd '../sindicancias'
docker build -t sindicancias .
echo '###########################################################'
echo '#Building docker sindicanciamaterialrodante               #'
echo '###########################################################'
cd '../sindicanciamaterialrodante'
docker build -t sindicanciamaterialrodante .
# echo '###########################################################'
# echo '#Building docker sindicanciaanalisestecnicas               #'
# echo '###########################################################'
# cd '../sindicanciaanalisestecnicas'
# docker build -t sindicanciaanalisestecnicas .
echo '###########################################################'
echo '#Building docker segurancapatrimonial               #'
echo '###########################################################'
cd '../segurancapatrimonial'
docker build -t segurancapatrimonial .
echo '###########################################################'
echo '#Building docker planoacao                                #'
echo '###########################################################'
cd '../planoacao'
docker build -t planoacao .
echo '###########################################################'
echo '#Building docker incidente                                #'
echo '###########################################################'
cd '../incidente'
docker build -t incidente .
echo '###########################################################'
echo '#Building docker financeirosindicancia                                #'
echo '###########################################################'
cd '../financeirosindicancia'
docker build -t financeirosindicancia .
echo '###########################################################'
echo '#Repositorios Building com sucesso!                       #'
echo '###########################################################'
sleep 10s