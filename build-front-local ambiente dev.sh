#!/bin/bash
echo on    


echo ###########################################################
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo ###########################################################
echo #Autor: Deovan Zanol                                      #
echo ###########################################################
echo ###########################################################
echo Buildando Frontend.
cd ../sro-frontend
npm install
npm run build -- --configuration=dev --base-href=//web --deploy-url=//
rm -r '../core-dockers/sro frontend/dist/'
cp -r ./dist  '../core-dockers/sro frontend/dist/'
cp nginx-custom.conf  '../core-dockers/sro frontend/'
cd ../core-dockers
echo ###########################################################
echo #Building docker!                                         #
echo ###########################################################
cd './sro frontend'
docker build -t frontend .
echo ###########################################################
echo #Build concluido!                                         #
echo ###########################################################

sleep 10s