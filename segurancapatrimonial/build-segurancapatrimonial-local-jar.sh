#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Building segurancapatrimonial.
cd ../../segurancapatrimonial
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers/segurancapatrimonial/app.jar
cd ../core-dockers
echo '###########################################################'
echo '#Building docker segurancapatrimonial                             #'
echo '###########################################################'
cd './segurancapatrimonial'
docker build -t segurancapatrimonial .
echo '###########################################################'
echo '#Repositorio buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s