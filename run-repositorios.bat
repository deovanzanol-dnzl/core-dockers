@echo on    
@echo ###########################################################
@echo #Script para rodar os repositorios do projeto da rumo.#
@echo ###########################################################
@echo Rodando sinistros.
docker run --rm -d -p 8060:8060/tcp sro_backend_sinistros:latest
@echo ###########################################################
@echo Rodando funcionarios.
docker build -f Dockerfile_build -t sro_backend_funcionarios .
@echo ###########################################################
@echo Rodando notificações.
docker build -f Dockerfile_build -t sro_backend_notificacoes .
@echo ###########################################################
@echo Rodando mercadorias.
docker build -f Dockerfile_build -t sro_backend_mercadorias .
@echo ###########################################################
@echo Rodando veiculosFerroviarios.
docker build -f Dockerfile_build -t sro_backend_veiculosferroviarios .
@echo ###########################################################
@echo Rodando macros.
docker build -f Dockerfile_build -t sro_backend_macros .
@echo ###########################################################
@echo Rodando subdivisoes.
docker build -f Dockerfile_build -t sro_backend_subdivisoes .
@echo ###########################################################
@echo Rodando sindicancias.
docker build -f Dockerfile_build -t sro_backend_sindicancias .
@echo ###########################################################
@echo Rodando sro-frontend.
docker build -t sro_frontend:qa-gft --build-arg configuration="qa-gft" .
@echo ###########################################################
timeout 5